# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

external_message_prefix = (
    "zsnl.v2.zsnl_communication_http_domains_communication.ExternalMessage."
)
