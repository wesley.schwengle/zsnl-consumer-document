pytest~=6.0
pytest-spec~=3.0
pytest-cov~=2.10
pytest-mock~=3.3
pip-audit
webtest
liccheck~=0.1

## Code style tools
black~=22.3
flake8~=4.0
isort~=5.10
flake8-bugbear~=22.4
