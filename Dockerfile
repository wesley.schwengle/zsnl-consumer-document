# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

#####################################################################
### Stage 1: Setup base python with requirements
#####################################################################
FROM python:3.9-slim-bullseye AS base

ARG BUILD_TIMESTAMP=undefined
ARG BUILD_REF=master

# Add necessary packages to system
RUN apt-get update && apt-get install -y \
  build-essential \
  libpq-dev \
  libmagic1 \
  git

COPY requirements/base.txt /tmp/requirements.txt

RUN python3 -m venv /opt/virtualenv \
  && . /opt/virtualenv/bin/activate \
  && pip install -r /tmp/requirements.txt

ENV PATH="/opt/virtualenv/bin:$PATH"

RUN echo "$BUILD_TIMESTAMP" > /opt/build-timestamp

RUN pip install git+https://gitlab.com/xxllnc/zaakgericht/zaken/zsnl-domains.git@$BUILD_REF

COPY . /opt/zsnl_documents_events_consumer
WORKDIR /opt/zsnl_documents_events_consumer

#####################################################################
## Stage 2: Production build
#####################################################################
FROM python:3.9-slim-bullseye AS production

ENV OTAP=production
ENV PYTHONUNBUFFERED=on
ENV PATH="/opt/virtualenv/bin:$PATH"
RUN apt-get update && apt-get install -y libmagic1 libpq5

# Set up application
COPY --from=base /opt /opt/

WORKDIR /opt/zsnl_documents_events_consumer
RUN pip install -e .

# Run as non-root user
USER 65534

#####################################################################
## Stage 3: QA environment
#####################################################################
FROM base AS quality-and-testing

ENV OTAP=test

COPY requirements/test.txt /tmp/requirements-test.txt
RUN pip install -r /tmp/requirements-test.txt

RUN  mkdir -p /root/test_result \
  && bin/git-hooks/pre-commit -c -j /root/test_result/junit.xml

#####################################################################
## Stage 4: Development image
#####################################################################
FROM quality-and-testing AS development

ENV OTAP=development

COPY requirements/dev.txt /tmp/requirements-dev.txt
RUN pip install -r /tmp/requirements-dev.txt
